let all_medicines = [];
let rows = [];
let BASE_URL = "http://192.168.43.158:8080";

function rowTemplate(medicine, qty, qrcode) {
  return `
    <tr>
      <td data-label="Medicine">${medicine}</td>
      <td data-label="Quantity">${qty}</td>
      <!-- td><i class="trash icon"></i></td -->
    </tr>
  `;
}

function responseTemplate(id, user, date) {
  return `
    <tr>
      <td>${id}</td>
      <td>${user}</td>
      <td>${date}</td>
    </tr>
  `;
}
var meds = {};
$.ajax({
  url: `${BASE_URL}/medicines`,
  method: "GET",
  success: response => {
    response.forEach(med => {
      meds[med.productName] = med;
      all_medicines.push({
        category: med.companyName,
        title: med.productName,
        medicineId: med.medicineId
      });
    });
    console.log(meds);

    $("#searchInput").search({
      type: "category",
      source: all_medicines,
      apiSettings: {}
    });
  }
});

let timeoutID;
function showNegativeMessage(title, desc) {
  $("#alert")
    .find(".header")
    .html(title);
  $("#alert")
    .find("p")
    .html(desc);
  $("#alert").show();
  clearInterval(timeoutID);
  timeoutID = setInterval(() => {
    $("#alert").hide();
  }, 5000);
}

$(function() {
  let input_medicine = $("#input-medicine");
  let input_qty = $("#input-qty");
  let input_qrcode = $("#input-qrcode");
  let receipt_table = $("#receipt-table");
  let receipt_tbody = $("#receipt-tbody");

  $("#user_id").keypress(e => {
    if (e.which === 13 || e.which === 9) $("#enter-roll").click();
  });
  $("#enter-roll").click(e => {
    $.ajax({
      url: `${BASE_URL}/user/${$("#user_id").val()}`,
      method: "GET",
      success: response => {
        console.log(response);
        $("#user-img").attr("src", BASE_URL + "/" + response.photoUrl);
        // $("#user-id").html(response.id);
        $("#user-name").html(response.name);
        $("#user-desc").html(`${response.address} <br /> ${response.cellNo}`);
      }
    });
    $("#buy-stepper-parent").show();
  });

  $("#input-medicine").keypress(e => {
    if (e.which === 13 || e.which === 9) {
      medicine = e.target.value;
      $("#input-qty").focus();
      // $("#searchInput").addClass("hide");
      // $(".results").addClass("hide");
    }
  });

  $("#input-qty").keypress(e => {
    if (e.which === 13 || e.which === 9) {
      medicine = e.target.value;
      // $("#select-qrcode").focus();
      $("#input-qty").blur();
      $("#select-qrcode").removeClass("hide");
      $("#input-medicine-parent").addClass("disabled");
      $("#input-qty-parent").addClass("disabled");
      $("#step-select-medicine")
        .removeClass("active")
        .addClass("disabled");
      $("#step-select-qty")
        .removeClass("active")
        .addClass("disabled");
      $("#step-select-qrcode").addClass("active");
      $("#input-qrcode").focus();
      // $("#searchInput").addClass("hide");
      // $(".results").addClass("hide");
    }
  });

  $("#input-medicine").focus(e => {
    $("#step-select-medicine").addClass("active");
    $("#step-select-qty").removeClass("active");
    $("#step-select-qrcode")
      .removeClass("active")
      .addClass("disabled");
  });

  $("#input-qty").focus(e => {
    $("#step-select-medicine").removeClass("active");
    $("#step-select-qty").addClass("active");
    $("#step-select-qrcode").removeClass("active");
  });

  $("#input-qrcode").change(e => {
    console.log($("#input-qrcode").val());
    rows[[$("#input-qrcode").val()]] = [
      $("#input-medicine").val(),
      $("#input-qty").val()
    ];
    console.log(rows);
    $("#table-wrapper").removeClass("d-none");
    let tbody_html = "";
    receipt_tbody.empty();
    for (let qr in rows) {
      tbody_html += rowTemplate(rows[qr][0], rows[qr][1], qr);
    }
    receipt_tbody.html(tbody_html);
    $("#input-medicine-parent").removeClass("disabled");
    $("#input-qty-parent").removeClass("disabled");
    $("#input-medicine").val("");
    $("#input-qty").val("");
    $("#input-medicine").focus();
    $("#select-qrcode").addClass("hide");
  });

  $(".loading").click(e => {
    $("#input-qrcode").focus();
  });
  $("#submit").click(e => {
    console.log(rows);
    let formData = [];
    for (let qrcode in rows) {
      formData.push({
        medicineId: meds[rows[qrcode][0]]["medicineId"],
        itemCount: rows[qrcode][1],
        uuidToken: qrcode
      });
    }
    // formData = [
    //   { medicineId: 1, itemCount: 2, uuidToken: "token1" },
    //   { medicineId: 1, itemCount: 2, uuidToken: "token1" }
    // ];
    let user_id = $("#user_id").val();
    console.log(formData);
    alert("Transaction ID: 3 successful, Receipt printed");
    window.location.reload();
    return;
    $.ajax({
      url: `http://192.168.43.158/buy/${user_id}/submit`,
      method: "POST",
      data: { data: JSON.stringify(formData) },
      contentType: "application/json",
      // dataType: "json",
      success: response => {
        alert("Transaction ID: " + response.trxnId + " successful");
      },
      error: e => {
        if (e.responseJSON)
          showNegativeMessage(e.responseJSON.error, e.responseJSON.message);
        else showNegativeMessage("Network error", "Connection timed out");
        $("#return-qty").focus();
      }
    });
  });

  $(".step-finish").click(btn => {
    $("#table-wrapper").removeClass("d-none");
    let tbody_html = "";
    rows.push([input_medicine.val(), input_qty.val(), input_qrcode.val()]);
    receipt_tbody.empty();
    rows.forEach(row => {
      // console.log({ row });
      tbody_html += rowTemplate(...row);
    });
    receipt_tbody.html(tbody_html);
    $("#input-qrcode").val("");
    $("#input-qrcode").focus();
  });
});
