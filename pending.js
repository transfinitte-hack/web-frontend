const BASE_URL = "http://192.168.43.158:8080";
function userRowTemplate(id, user, date) {
  return `
    <tr>
      <td>${id}</td>
      <td>${user}</td>
      <td>${date}</td>
    </tr>
  `;
}
let timeoutID;
function showNegativeMessage(title, desc) {
  $("#alert")
    .find(".header")
    .html(title);
  $("#alert")
    .find("p")
    .html(desc);
  $("#alert").show();
  clearInterval(timeoutID);
  timeoutID = setInterval(() => {
    $("#alert").hide();
  }, 5000);
}

$(function() {
  $.ajax({
    url: `${BASE_URL}/return/pending`,
    method: "GET",
    success: response => {
      console.log(response);
      let html = "";
      for (let txn of response) {
        html += userRowTemplate(
          txn.trnxId,
          txn.returnedByUserId,
          txn.returnDate
        );
      }
      $("#txn-body").html(html);
    },
    error: e => {
      if (e.responseJSON)
        showNegativeMessage(e.responseJSON.error, e.responseJSON.message);
      else showNegativeMessage("Network error", "Connection timed out");
      $("#return-qty").focus();
    }
  });
});
