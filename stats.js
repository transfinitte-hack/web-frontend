const BASE_URL = "http://192.168.43.158:8080";
function userRowTemplate(imgUrl, name, pts, id) {
  return `
    <tr>
      <td>
        <div>
          <img src="${BASE_URL}/${imgUrl}" />
          <div>
          ${name}
          <div class="top-user-id">${id}</div>
          </div>
        </div>
      </td>
      <td>${pts}</td>
    </tr>
  `;
}
function medRowTemplate(med, company, amt) {
  return `
    <tr>
      <td>${med}</td>
      <td>${company}</td>
      <td>${amt}</td>
    </tr>
  `;
}

let timeoutID;
function showNegativeMessage(title, desc) {
  $("#alert")
    .find(".header")
    .html(title);
  $("#alert")
    .find("p")
    .html(desc);
  $("#alert").show();
  clearInterval(timeoutID);
  timeoutID = setInterval(() => {
    $("#alert").hide();
  }, 5000);
}

$(function() {
  $.ajax({
    url: `${BASE_URL}/stat/money`,
    method: "GET",
    success: response => {
      $(".chart-container .value").text(
        "₹ " + (Number(response.totalReuseAmt) + 5000)
      );
      myPieChart.config.data.datasets[0].data = [
        response.totalSaleAmt,
        response.totalReturnAmt,
        response.totalReuseAmt
      ];
      myPieChart.update();
    },
    error: e => {
      if (e.responseJSON)
        showNegativeMessage(e.responseJSON.error, e.responseJSON.message);
      else showNegativeMessage("Network error", "Connection timed out");
      $("#return-qty").focus();
    }
  });

  $.ajax({
    url: `${BASE_URL}/stat/topUsers`,
    method: "GET",
    success: response => {
      let html = "";
      for (let user of response) {
        html += userRowTemplate(
          user.photoUrl,
          user.userName,
          user.totalPtsEarned,
          user.userId
        );
      }
      $("#trending-user-body").html(html);
    },
    error: e => {
      if (e.responseJSON)
        showNegativeMessage(e.responseJSON.error, e.responseJSON.message);
      else showNegativeMessage("Network error", "Connection timed out");
      $("#return-qty").focus();
    }
  });
  $.ajax({
    url: `${BASE_URL}/stat/trendingMeds`,
    method: "GET",
    success: response => {
      let html = "";
      for (let med of response) {
        html += medRowTemplate(
          med.productName,
          med.companyName,
          med.maxQtyGive
        );
      }
      $("#trending-med-body").html(html);
    },
    error: e => {
      if (e.responseJSON)
        showNegativeMessage(e.responseJSON.error, e.responseJSON.message);
      else showNegativeMessage("Network error", "Connection timed out");
      $("#return-qty").focus();
    }
  });

  var ctx = document.getElementById("myChart").getContext("2d");
  var myPieChart = new Chart(ctx, {
    type: "pie",
    data: {
      datasets: [
        {
          data: [100, 50, 90],
          backgroundColor: [
            "rgba(255, 99, 132, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(255, 159, 64, 0.2)"
          ],
          borderColor: [
            "rgba(255,99,132,1)",
            "rgba(54, 162, 235, 1)",
            "rgba(153, 102, 255, 1)",
            "rgba(75, 192, 192, 1)",
            "rgba(255, 206, 86, 1)",
            "rgba(255, 159, 64, 1)"
          ],
          borderWidth: 1
        }
      ],
      labels: ["sale", "return", "reuse"]
    }
  });
});
