let rows = [];
let BASE_URL = "http://192.168.43.158:8080";

function rowTemplate(medicine, qty, qrcode) {
  return `
    <tr>
      <td data-label="Medicine">${medicine}</td>
      <td data-label="Quantity">${qty}</td>
      <!-- td><i class="trash icon"></i></td -->
    </tr>
  `;
}

let timeoutID;
function showNegativeMessage(title, desc) {
  $("#alert")
    .find(".header")
    .html(title);
  $("#alert")
    .find("p")
    .html(desc);
  $("#alert").show();
  clearInterval(timeoutID);
  timeoutID = setInterval(() => {
    $("#alert").hide();
  }, 5000);
}

function tableTemplate(med) {
  return `
    <tr>
      <td>${med.productName}</td>
      <td>${med.companyName}</td>
      <td>${med.expiryDate}</td>
      <td>${med.return_qty}</td>
      <td>${med.action.split("_").join(" ")}</td>
    </tr>
  `;
}

function refreshTable() {
  console.log(returned_meds);
  if (
    Object.entries(returned_meds).length > 0 &&
    returned_meds.constructor === Object
  )
    $("#table-wrapper").removeClass("d-none");
  $("#receipt-tbody").empty();
  let tbody_html = "";
  for (let uuid in returned_meds) {
    tbody_html += tableTemplate(returned_meds[uuid]);
  }
  $("#receipt-tbody").html(tbody_html);
}

let returned_meds = {};
let current_uuid = "";

Date.prototype.addDays = function(days) {
  this.setDate(this.getDate() + parseInt(days));
  return this;
};

$(function() {
  $("#input-qrcode").change(e => {
    console.log($("#input-qrcode").val());
    $.ajax({
      url: `${BASE_URL}/return/qrVerify`,
      method: "POST",
      data: $("#input-qrcode").val(),
      success: response => {
        console.log(response);
        current_uuid = response.uuid;
        returned_meds[response.uuid] = response;
        $("#med-name").html(
          `${response.productName} (${response.companyName})`
        );
        $("#modal-desc").html(
          `Brought by ${response.boughtByUserId} on ${
            response.buyDate.split("T")[0]
          }`
        );
        $("#return-qty").attr("max", response.itemCount);
        $(".ui.modal").modal("show");
        let exp_date = new Date(response.expiryDate);
        exp_date.addDays(50);
        let now = new Date();
        if (exp_date < now) {
          $("#add-to-inventory").hide();
          $(".modal-error").show();
        } else {
          $("#add-to-inventory").show();
          $(".modal-error").hide();
        }
        $("#return-qty").focus();
      },
      error: e => {
        if (e.responseJSON)
          showNegativeMessage(e.responseJSON.error, e.responseJSON.message);
        else showNegativeMessage("Network error", "Connection timed out");
        $("#return-qty").focus();
      }
    });
  });

  $("#add-to-inventory").click(e => {
    returned_meds[current_uuid].action = "ADD_TO_INVENTORY";
    returned_meds[current_uuid].return_qty = $("#return-qty").val();
    $(".ui.modal").modal("hide");
    refreshTable();
    $("#return-qty").focus();
    $("#return-qty").val("");
  });

  $("#qr-container > div > span").click(e => {
    $("#input-qrcode").focus();
    $("#input-qrcode").val("");
  });

  $("#dispose").click(e => {
    returned_meds[current_uuid].action = "DISPOSE";
    returned_meds[current_uuid].return_qty = $("#return-qty").val();
    $(".ui.modal").modal("hide");
    refreshTable(returned_meds[current_uuid]);
    $("#return-qty").focus();
    $("#return-qty").val("");
  });

  $("#decide-later").click(e => {
    returned_meds[current_uuid].action = "DECIDE_LATER";
    returned_meds[current_uuid].return_qty = $("#return-qty").val();
    $(".ui.modal").modal("hide");
    refreshTable(returned_meds[current_uuid]);
    $("#return-qty").focus();
    $("#return-qty").val("");
  });

  $("#user_id").keypress(e => {
    if (e.which === 13 || e.which === 9) {
      $("#enter-roll").click();
    }
  });

  $("#submit").click(e => {
    alert("Transaction ID: 3 successful, Receipt printed");
    window.location.reload();
    return;
    $.ajax({
      url: `http://192.168.43.158/buy/${user_id}/submit`,
      method: "POST",
      data: { data: JSON.stringify(formData) },
      success: response => {
        alert("Transaction ID: " + response.trxnId + " successful");
      },
      error: e => {
        if (e.responseJSON)
          showNegativeMessage(e.responseJSON.error, e.responseJSON.message);
        else showNegativeMessage("Network error", "Connection timed out");
        $("#return-qty").focus();
      }
    });
  });

  $("#enter-roll").click(e => {
    $.ajax({
      url: `${BASE_URL}/user/${$("#user_id").val()}`,
      method: "GET",
      success: response => {
        console.log(response);
        $(".ui.cards").removeClass("d-none");
        $("#qr-container").removeClass("d-none");
        $("#user-img").attr("src", BASE_URL + "/" + response.photoUrl);
        // $("#user-id").html(response.id);
        $("#user-name").html(response.name);
        $("#user-desc").html(`${response.address} <br /> ${response.cellNo}`);
        $("#input-qrcode").focus();
        $("#input-qrcode").val("");
      },
      error: e => {
        if (e.responseJSON)
          showNegativeMessage(e.responseJSON.error, e.responseJSON.message);
        else showNegativeMessage("Network error", "Connection timed out");
        $("#input-qrcode").focus();
        $("#input-qrcode").val("");
      }
    });
    $("#buy-stepper-parent").show();
  });
});
